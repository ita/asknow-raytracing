# Installation and start
- Install Unity 2020.2.3f with WebGL Package
- open the this folder into unity, might take some time to open
- open the Scenes folder below
- open `RayTracer`
- with the playbutton at the top you can run it in the editor

# Change Colours
To change the colour, the colour in the Sprites Folder need to be updated,
- for the walls in blue the DarkNavy colour need to be changed
- for the button the `Group 57Reset`
- for the circles and the labels, click on the element in the Scene Window and on the left side you can choose the colour.
- the background can be changed in the `Main Camera` Game Object on the left side

# Build it
- File -> Build Settings
- Choose Platform WebGL and press Switch Platform, if WebGL is not chosen
- Build an run choose the build folder (Usually root/builds/anyBuildName)
- wait and it should open automatically in the browser
