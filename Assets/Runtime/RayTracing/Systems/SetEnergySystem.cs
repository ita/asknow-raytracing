using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(TracingSystemGroup))]
[UpdateAfter(typeof(MoveRaySystem))]
public class SetEnergySystem : SystemBase
{
   
    protected override void OnUpdate()
    {
        Entities.ForEach((ref NonUniformScale scale,in MoveRay moveTo) =>
        {
            if(moveTo.Speed==0)
                return;
            var distance = moveTo.Distance;
            var energy =moveTo.Energy/ distance*4;
            // energy = 1f/(-10f * math.log10(energy));
            energy = energy > 0.5f ? 0.5f : energy;
            scale.Value=new float3(energy,energy,energy);
        }).ScheduleParallel();
    }

}