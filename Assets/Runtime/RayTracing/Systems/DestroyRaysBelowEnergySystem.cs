using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

[UpdateInGroup(typeof(TracingSystemGroup))]
[UpdateAfter(typeof(MoveRaySystem))]
public class DestroyRaysBelowEnergySystem : SystemBase
{
    EndSimulationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
    
        Entities
            .WithName("RaySpawner")
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, ref NonUniformScale scale,in MoveRay moveTo) =>
            {
                var distance = moveTo.Distance;
                distance = distance < 1f ? 1f : distance;
                var energy =moveTo.Energy/ distance;
                if(energy<0.0000001f)
                    commandBuffer.DestroyEntity(entityInQueryIndex,entity);
            }).ScheduleParallel();

        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}