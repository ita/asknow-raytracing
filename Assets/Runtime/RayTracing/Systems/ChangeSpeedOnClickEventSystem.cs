using Unity.Burst;
using Unity.Entities;

[UpdateInGroup(typeof(TracingSystemGroup))]
public class ChangeSpeedOnClickEventSystem : SystemBase
{
   
    protected override void OnUpdate()
    {
        var add = 0f;
        Entities.WithAll<ChangeSpeedData,OnClickedTag>()
            .ForEach((Entity entity, int entityInQueryIndex,in ChangeSpeedData tag) =>
            {
                add = tag.Value;
            }).Run();
        if(add==0)
            return;
        Entities
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((ref SpeedData speedData) =>
            {
                if(speedData.Value + add>0&&speedData.Value + add<5f)
                    speedData.Value += add; 
            }).Run();
    }
}