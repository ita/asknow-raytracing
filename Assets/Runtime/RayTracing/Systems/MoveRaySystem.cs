using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateInGroup(typeof(TracingSystemGroup))]
[UpdateAfter(typeof(ChangeSpeedOnClickEventSystem))]
public class MoveRaySystem : SystemBase
{
    private const float XLimitLow = -9.13f+0.15f;
    private const float XLimitUp1 = 0.8699997f-0.15f;
    private const float XLimitUp2 = -2.13f-0.15f;
    private const float YLimitLow = -3.95f+0.15f;
    private const float YLimitUp = 4.05f-0.15f;
    private const float MiddleLimit = 0.05f-0.15f;
    protected override void OnUpdate()
    {
        var speed = 0f;
        Entities.ForEach((in SpeedData speedData) =>
        {
            speed = speedData.Value;
        }).Run();
        var timeDeltaTime = Time.DeltaTime;
        Entities.ForEach((ref MoveRay moveTo, ref Translation translation) =>
        {
            if(moveTo.Speed==0)
                return;
            var deltaWay = moveTo.Direction * moveTo.Speed * timeDeltaTime*speed;
            var moveToDirection = translation.Value+ deltaWay;
            if (moveToDirection.x < XLimitLow)
            {
                moveToDirection.x = XLimitLow;
                var normal = new float3(1,0, 0);
                UpdateMoveTo(normal, ref moveTo);
            }
            else if (moveToDirection.y < YLimitLow)
            {
                moveToDirection.y = YLimitLow;
                var normal = new float3(0,1, 0);
                UpdateMoveTo(normal, ref moveTo);
            }
            else if (moveToDirection.y > YLimitUp)
            {
                moveToDirection.y = YLimitUp;
                var normal = new float3(0,1, 0);
                UpdateMoveTo(normal, ref moveTo);
            }
            // horizontal Plane
            else if (moveToDirection.y >= MiddleLimit && 
                     (true || translation.Value.x < XLimitUp1) &&
                     translation.Value.x > XLimitUp2 && 
                     (true || moveToDirection.x < XLimitUp1) && 
                     moveToDirection.x > XLimitUp2 &&
                     moveToDirection.y - MiddleLimit < 1f)
            {
                if (moveToDirection.y - MiddleLimit > 1f)
                {
                    var a = 0;
                    a++;
                }

                moveToDirection.y = MiddleLimit;
                var normal = new float3(0, -1, 0);
                UpdateMoveTo(normal, ref moveTo);
            }
            else if(moveToDirection.x > XLimitUp1 && translation.Value.y < MiddleLimit && moveToDirection.y < MiddleLimit && moveToDirection.x - XLimitUp1 < 1f )
            {
                if (moveToDirection.x - XLimitUp1 > 1f)
                {
                    var a = 0;
                    a++;
                }
                moveToDirection.x = XLimitUp1;
                var normal = new float3(-1, 0, 0);
                UpdateMoveTo(normal, ref moveTo);
            }
            else if(moveToDirection.x > XLimitUp2 && translation.Value.y >= MiddleLimit && moveToDirection.y >= MiddleLimit && moveToDirection.x - XLimitUp2 < 1f)
            {
                if (moveToDirection.x - XLimitUp2 > 1f)
                {
                    var a = 0;
                    a++;
                }
                moveToDirection.x = XLimitUp2;
                var normal = new float3(-1, 0, 0);
                UpdateMoveTo(normal, ref moveTo);
            }

            moveTo.Distance += math.length(deltaWay);
            translation.Value = moveToDirection;
        }).ScheduleParallel();
    }

    private static void UpdateMoveTo(float3 normal, ref MoveRay moveRay)
    {
        moveRay.Direction = math.reflect(moveRay.Direction, normal);
        moveRay.Energy *= 0.9f;
    }
}