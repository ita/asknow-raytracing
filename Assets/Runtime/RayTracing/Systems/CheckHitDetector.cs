using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using VBAecs.Histogram;

[UpdateInGroup(typeof(TracingSystemGroup))]
[UpdateAfter(typeof(MoveRaySystem))]
public class CheckHitDetector : SystemBase
{
    EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;
    protected override void OnCreate()
    {
        base.OnCreate();
        // Find the ECB system once and store it for later usage
        m_EndSimulationEcbSystem = World
            .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        var receiverTranslation = float3.zero;
        var receiverRadius = 0f;
        var deltaWay = 0f;
        var zeroPos = float3.zero;
        Entities.ForEach((in HistogramData histogram) =>
        {
            deltaWay = histogram.DeltaWay;
            zeroPos = histogram.Zero;
        }).Run();
        

        Entities.ForEach((in ReceiverDetector rec, in Translation translation) =>
        {
            receiverTranslation = translation.Value;
            receiverRadius = rec.Radius;
        }).Run();

      
        var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
        Entities
            .ForEach((Entity entity, int entityInQueryIndex, ref MoveRay moveTo, in Translation translation, in NonUniformScale scale) =>
            {
                var distance = math.length(receiverTranslation - translation.Value);
                if (distance < receiverRadius)
                {
                    //hit 
                    moveTo.Speed = 0f;
                    var goal = zeroPos;
                    var bla = moveTo.Distance/4;
                    goal.x += bla * deltaWay;
                    var energy = moveTo.Energy / moveTo.Distance;
                    var newTrans = receiverTranslation;
                    newTrans.x = goal.x;
                    newTrans.y = 4.5f;
                    ecb.SetComponent(entityInQueryIndex, entity,
                        new Translation()
                            {Value = newTrans});
                    ecb.AddComponent(entityInQueryIndex, entity,
                        new MoveInHistogram() {Energy = energy, Position = goal, Speed = 2f});
                    ecb.RemoveComponent(entityInQueryIndex, entity, typeof(MoveRay));
                }
            }).ScheduleParallel();
        
        m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
    }
}