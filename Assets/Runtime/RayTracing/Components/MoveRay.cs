using Unity.Entities;
using Unity.Mathematics;

public struct MoveRay : IComponentData
{
    public float3 Direction;
    public float Speed;
    public float Energy;
    public float Distance;
}