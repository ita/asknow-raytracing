using Unity.Entities;

[GenerateAuthoringComponent]
public struct ChangeSpeedData : IComponentData
{
    public float Value;
}