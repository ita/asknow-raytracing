using Unity.Entities;

[GenerateAuthoringComponent]
public struct ReceiverDetector : IComponentData
{
    public float Radius;
}