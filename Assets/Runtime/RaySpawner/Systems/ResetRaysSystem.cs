using Unity.Burst;
using Unity.Entities;
using VBAecs.Histogram;

public class ResetRaysSystem : SystemBase
{
    private BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate()
    {
        // Cache the BeginInitializationEntityCommandBufferSystem in a field, so we don't have to create it every frame
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var removeRays = false;
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        Entities.WithAll<RemoveRaysTag,OnClickedTag>()
            .ForEach((Entity entity, int entityInQueryIndex) =>
            {
                removeRays = true;
            }).Run();
        
        if(!removeRays)
            return;

        Entities.WithAny<MoveRay,MoveInHistogram>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex) =>
            {
                commandBuffer.DestroyEntity(entityInQueryIndex,entity);
            }).ScheduleParallel();

        Entities.WithAll<RemoveRaysTag,OnClickedTag>()
            .ForEach((Entity entity, int entityInQueryIndex,in RemoveRaysTag tag) =>
            {
                commandBuffer.RemoveComponent(entityInQueryIndex, entity, typeof(OnClickedTag));
            }).ScheduleParallel();
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }
}