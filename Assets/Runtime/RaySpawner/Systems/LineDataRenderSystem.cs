using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateAfter(typeof(LineDataSetSystem))]
public class LineDataRenderSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.WithChangeFilter<LineData>().ForEach((ref Translation translation,ref NonUniformScale scale, ref Rotation rotation,
            in LineData lineData) =>
        {
            if (!lineData.IsActive)
            {
                translation.Value=new float3(-10,-10,0);

                return;
            }
            var start = lineData.StartPoint;
            var end = lineData.EndPoint;
            
            var diff = (end-start);
            var length = math.distance(end,start);
            translation.Value = start;
            length = 1f;
            var angleAlpha = math.acos(math.dot(start, end) / (math.length(end) * math.length(start)));
            angleAlpha = -math.atan2(diff.x,diff.y);
            rotation.Value = ToQuaternion(angleAlpha,0,0);
            scale.Value.y = length;
        }).ScheduleParallel();
    }
    
    private static quaternion ToQuaternion(float yaw, float pitch, float roll) // yaw (Z), pitch (Y), roll (X)
    {
        // Abbreviations for the various angular functions
        var cy = math.cos(yaw * 0.5f);
        var sy = math.sin(yaw * 0.5f);
        var cp = math.cos(pitch * 0.5f);
        var sp = math.sin(pitch * 0.5f);
        var cr = math.cos(roll * 0.5f);
        var sr = math.sin(roll * 0.5f);

        var w = cr * cp * cy + sr * sp * sy;
        var x = sr * cp * cy - cr * sp * sy;
        var y = cr * sp * cy + sr * cp * sy;
        var z = cr * cp * sy - sr * sp * cy;

        return new quaternion(x,y,z,w);
    }
}