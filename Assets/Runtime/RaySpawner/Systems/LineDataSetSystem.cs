using CodeMonkey.Utils;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class LineDataSetSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var startPos = float3.zero;
        Entities.WithAll<SourceTag>().ForEach((in Translation translation) =>
        {
            startPos=translation.Value;
        }).Run();
        var endPos = float3.zero;
        var active = false;
        var mousePos = new float3(UtilsClass.GetMouseWorldPosition());
        Entities.WithAll<OnHoverOverTag,RoomData>().ForEach((in Translation translation) =>
        {
            endPos=mousePos;
            active = true;
        }).Run();
        Entities.ForEach((ref LineData lineData) =>
        {
            lineData.IsActive = active;
            lineData.StartPoint = startPos;
            lineData.EndPoint = endPos;

        }).Run();
    }
}