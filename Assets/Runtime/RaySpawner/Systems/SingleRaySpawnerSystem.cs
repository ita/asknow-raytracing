using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class SingleRaySpawnerSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnUpdate()
    {
        var positionMouse = new float3(0,0,0);
        var startRays = false;
        Entities.ForEach((ref InputPointMouse inputPoint) =>
        {
            positionMouse = inputPoint.Position;
            if (inputPoint.NewRay)
            {
                inputPoint.NewRay = false;
                startRays = true;
            }
        }).Run();

        if (startRays)
        {
            var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

            Entities.WithNone<StartRandomRays>()
                .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
                .ForEach((Entity entity, int entityInQueryIndex, in RaySpawner spawnerFromEntity,in Translation translation) =>
                {
                    var instance = commandBuffer.Instantiate(entityInQueryIndex, spawnerFromEntity.Prefab);

                    var position = translation.Value;
                    var moveDirection = positionMouse - position;
                    moveDirection = math.normalize(moveDirection);
                    commandBuffer.SetComponent(entityInQueryIndex, instance, new Translation {Value = position});
                    commandBuffer.AddComponent(entityInQueryIndex, instance,
                            new MoveRay() {Direction = moveDirection, Speed = 1f,Distance = 0f,Energy = 1f});

                }).ScheduleParallel();

            m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
    }
}