using System.Numerics;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class MultipleRaySpawnerSystem : SystemBase
{
    BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
    EntityQuery simulationTimeQuery;
    Random random;

    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();

        random = Random.CreateFromIndex(9129279);
    }


    protected override void OnUpdate()
    {
        var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        var prefab = Entity.Null;
        var position = float3.zero;
        Entities
            .ForEach((in RaySpawner spawnerFromEntity,in Translation translation) =>
            { 
                prefab = spawnerFromEntity.Prefab;
                position = translation.Value;
            }).Run();

        if (prefab == Entity.Null) return;
        random.NextFloat();
        var random1 = random;
        Entities.WithAll<OnClickedTag>()
            .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
            .ForEach((Entity entity, int entityInQueryIndex, in StartRandomRays startRandomRays) =>
            {
                for (var i = 0; i < startRandomRays.Number; i++)
                {
                    var angle = random1.NextFloat(0, 360);
                    var instance = commandBuffer.Instantiate(entityInQueryIndex, prefab);
                    var moveDirection = new float3(math.cos(angle * i), math.sin(angle * i), 0);
                    commandBuffer.SetComponent(entityInQueryIndex, instance,
                        new Translation {Value = position});

                    commandBuffer.AddComponent(entityInQueryIndex, instance,
                        new MoveRay() {Direction = moveDirection, Speed = 1f, Distance = 0f, Energy = 1f});
                }


            }).ScheduleParallel();
        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
    }

}