using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct RaySpawner : IComponentData { 
    public Entity Prefab;
}