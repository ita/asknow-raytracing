using Unity.Entities;
using Unity.Mathematics;

public struct RaySeriesSpawner : IComponentData { 
    public Entity Prefab;
    public float3 Direction;
    public float SpawnsPerSeconds;
    public float TimeSinceLastSpawn;
    public int TotalNumber;
}