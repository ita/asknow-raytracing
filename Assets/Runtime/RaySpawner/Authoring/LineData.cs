using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct LineData : IComponentData
{
    public bool IsActive;
    public float3 StartPoint;
    public float3 EndPoint;
}