using Unity.Entities;

[GenerateAuthoringComponent]
public struct StartRandomRays : IComponentData
{
    public int Number;
}