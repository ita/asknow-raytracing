using Unity.Entities;

namespace VBAecs.Histogram
{
    [GenerateAuthoringComponent]
    public struct HistogramTag : IComponentData { }
}