using Unity.Entities;
using Unity.Mathematics;

namespace VBAecs.Histogram
{
    public struct MoveInHistogram : IComponentData
    {
        public float3 Position;
        public float3 LastMoveDirection;
        public float Speed;
        public float Energy;
    }
}