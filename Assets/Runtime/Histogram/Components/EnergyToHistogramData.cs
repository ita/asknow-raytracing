using Unity.Entities;

namespace VBAecs.Histogram
{
    public struct EnergyToHistogramData : IComponentData
    {
        public float Energy;

    }
}