using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

namespace VBAecs.Histogram
{
    [UpdateInGroup(typeof(HistogramSystemGroup))]
    public class HistogramResetterSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var removeRays = false;
            Entities.WithAll<RemoveRaysTag,OnClickedTag>()
                .ForEach((Entity entity, int entityInQueryIndex,in RemoveRaysTag tag) =>
                {
                    removeRays = true;
                }).Run();
            if(!removeRays)
                return;
            Entities.WithAll<HistogramTag>()
                .WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
                .ForEach((ref NonUniformScale scale) => { scale.Value.y = 0; }).ScheduleParallel();
        }
    }
}