using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace VBAecs.Histogram
{
    [UpdateInGroup(typeof(HistogramSystemGroup))]
    public class MoveTowardsSystem : SystemBase
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            // Find the ECB system once and store it for later usage
            m_EndSimulationEcbSystem = World
                .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {

            var timeDeltaTime = Time.DeltaTime;
            var ecb = m_EndSimulationEcbSystem.CreateCommandBuffer().AsParallelWriter();
            Entities.ForEach((Entity entity, int entityInQueryIndex, ref MoveInHistogram moveTo,
                ref Translation translation) =>
            {
                if (moveTo.Speed == 0)
                    return;
                var reachedPositionDistance = 0.1f;
                if (-(translation.Value.x - moveTo.Position.x) > reachedPositionDistance)
                {
                    moveTo.LastMoveDirection = new float3(1, 0, 0);
                    translation.Value += moveTo.LastMoveDirection * moveTo.Speed * timeDeltaTime;
                }
                else if ((translation.Value.y - moveTo.Position.y) > reachedPositionDistance)
                {
                    moveTo.LastMoveDirection = new float3(0, -1, 0);
                    translation.Value += moveTo.LastMoveDirection * moveTo.Speed * timeDeltaTime;
                }
                else
                {
                    ecb.RemoveComponent<MoveInHistogram>(entityInQueryIndex, entity);
                    ecb.AddComponent(entityInQueryIndex, entity, new EnergyToHistogramData() {Energy = moveTo.Energy});
                }
            }).ScheduleParallel();

            m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
        }

        private static void UpdateMoveTo(float3 normal, ref MoveRay moveRay)
        {
            moveRay.Direction = math.reflect(moveRay.Direction, normal);
            moveRay.Energy *= 0.9f;
        }
    }
}