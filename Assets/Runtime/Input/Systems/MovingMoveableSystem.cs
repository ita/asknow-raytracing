using CodeMonkey.Utils;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateInGroup(typeof(InputGuiSystemGroup))]
public class MovingMoveableSystem : SystemBase
{
    BeginSimulationEntityCommandBufferSystem m_EntityCommandBufferSystem;
    
    protected override void OnCreate()
    {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var  mousePos1 = new float3(UtilsClass.GetMouseWorldPosition());

        var moved = false;
        Entities.WithAll<MoveableTag,OnClickedPressTag>()
            .ForEach((ref Translation translation) =>
            {
                translation.Value = mousePos1;
                moved = true;
            }).Run();
       
        
        if (moved)
        {
            var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
            Entities.WithAll<RemoveRaysTag>()
                .ForEach((Entity entity, int entityInQueryIndex, in RemoveRaysTag tag) =>
                {
                    commandBuffer.AddComponent(entityInQueryIndex, entity, new OnClickedTag());
                }).ScheduleParallel();
            m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}