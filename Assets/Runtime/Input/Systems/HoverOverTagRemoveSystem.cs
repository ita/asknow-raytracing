using CodeMonkey.Utils;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
[UpdateInGroup(typeof(InputGuiSystemGroup))]

[UpdateBefore(typeof(ClickedTagAddSystem))]
public class HoverOverTagRemoveSystem : SystemBase
{
    private const float XLimitLow = -9.13f+0.15f;
    private const float XLimitUp1 = 0.8699997f-0.15f;
    private const float XLimitUp2 = -2.13f-0.15f;
    private const float YLimitLow = -3.95f+0.15f;
    private const float YLimitUp = 4.05f-0.15f;
    private const float MiddleLimit = 0.05f-0.15f;
    
    private BeginSimulationEntityCommandBufferSystem _entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }
    
    protected override void OnUpdate()
    {
        var mousePosition = new float3(UtilsClass.GetMouseWorldPosition());
        var isMouseOutOfRoom = (mousePosition.x < XLimitLow) || (mousePosition.y < YLimitLow) ||
                            (mousePosition.y > YLimitUp) ||
                            (mousePosition.y >= MiddleLimit && mousePosition.x < XLimitUp1 &&
                             mousePosition.x > XLimitUp2 && mousePosition.y - MiddleLimit < 1) ||
                            (mousePosition.x > XLimitUp1 && mousePosition.y < MiddleLimit) ||
                            (mousePosition.x > XLimitUp2 && mousePosition.y >= MiddleLimit);
        var commandBuffer = _entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
        // var isOnSomething = false;
        var startPos = new float3(UtilsClass.GetMouseWorldPosition());
     
        
        
        // Remove Tag
        Entities.WithAll<OnHoverOverTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation translation,
            in ClickableSquare inputPoint) =>
        {
            if (!(math.abs((startPos - translation.Value).x) < inputPoint.Scale.x / 2
                  && math.abs((startPos - translation.Value).y) < inputPoint.Scale.y / 2
                  && math.abs((startPos - translation.Value).z) < inputPoint.Scale.z / 2))
            {
                commandBuffer.RemoveComponent<OnHoverOverTag>( entityInQueryIndex, entity);
                //Debug.Log("ClickableSquare.RemoveComponent(entity, new OnHoverOverTag())");
            }
            else
            {
                //isOnSomething = true;
            }
        }).ScheduleParallel();
        Entities.WithAll<OnHoverOverTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation translation,
            in ClickableCircle inputPoint) =>
        {
            if (!(math.length(startPos - translation.Value) < inputPoint.Radius))
            {
                commandBuffer.RemoveComponent<OnHoverOverTag>(entityInQueryIndex, entity);
                //Debug.Log("ClickableCircle.RemoveComponent(entity, new OnHoverOverTag())");
            }
            else
            {
                //isOnSomething = true;
            }
        }).ScheduleParallel();
        
        if (isMouseOutOfRoom)
        {
            Entities.WithAll<OnHoverOverTag>().ForEach((Entity entity, int entityInQueryIndex, ref RoomData translation) =>
            {
                commandBuffer.RemoveComponent<OnHoverOverTag>(entityInQueryIndex, entity);
                //Debug.Log("RoomData.RemoveComponent(entity, new OnHoverOverTag())");
            }).ScheduleParallel();
        }
        
        _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
        
    }

}