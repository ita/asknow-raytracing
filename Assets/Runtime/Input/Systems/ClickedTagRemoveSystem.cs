using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(InputGuiSystemGroup))]
public class ClickedTagRemoveSystem : SystemBase
{
    private BeginSimulationEntityCommandBufferSystem _entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        //if (!Input.GetMouseButtonUp(0)) return;
        
        var commandBuffer = _entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

        Entities.WithAll<OnClickedTag>().ForEach((Entity entity, int entityInQueryIndex) =>
        {
            commandBuffer.RemoveComponent<OnClickedTag>(entityInQueryIndex, entity);
        }).ScheduleParallel();
        _entityCommandBufferSystem.AddJobHandleForProducer(Dependency);

    }
}