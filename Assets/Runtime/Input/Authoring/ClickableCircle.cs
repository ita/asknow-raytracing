using Unity.Entities;

[GenerateAuthoringComponent]
public struct ClickableCircle : IComponentData
{
    public float Radius;
}